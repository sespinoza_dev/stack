const R = require('ramda')

class Stack {
  constructor(elements){
    this.stack = elements
  }

  getAll() {
    return this.stack
  }

  push(item) {
    this.stack.push(item)
  }

  pop() {
    return this.stack.pop()
  }

  top() {
    return this.stack[this.stack.length -1]
  }

  isEmpty() {
    return this.stack.length == 0 ? true: false
  }
  
  empty() {
    return this.stack = []
  }

  sortBy(prop) {
    if (!R.has(prop)(this.stack[0])) {
      throw new Error(`attribute ${prop} does not exist`)
    }
    const updated = R.sortBy(R.prop(prop))(this.stack)
    this.stack = updated
    return this.stack
  }

}

module.exports = Stack;