const { expect } = require('chai')
const Stack      = require('../index')

describe('Stack', () => {
  describe('getAll', () => {
    it('should return all the elemets of the stack', done => {
      const stack = new Stack([1])
      const actual = stack.getAll()
      expect(actual).to.be.deep.equal([1])
      done()
    })

    it('shoul return empty array', done => {
      const stack = new Stack([])
      const actual = stack.getAll()
      expect(actual).to.be.deep.equal([])
      done()
    })
  })
  
  describe('push', () => {
    it('should push an element to top of the stack', done => {
      const stack = new Stack([1,2])
      stack.push(3)
      const actual = stack.getAll()
      expect(actual).to.be.deep.equal([1,2,3])
      done()
    })
  })
  
  describe('pop', () => {
    it('shoul return the first element of the stack and remove it', done => {
      const stack = new Stack([1,2])
      const element = stack.pop()
      expect(element).to.be.equal(2)
      const actual = stack.getAll()
      expect(actual).to.be.deep.equal([1])
      done()
    })
  })
  
  describe('top', () => {
    it('shoul return the top element of the stack without removing it', done => {
      const stack = new Stack([1,2])
      const element = stack.top()
      expect(element).to.be.deep.equal(2)
      const actual = stack.getAll()
      expect(actual).to.be.deep.equal([1,2])
      done()
    })
  })
  
  describe('isEmpty', () => {
    it('should return true if the stack is empty', done => {
      const stack = new Stack([])
      const actual = stack.isEmpty()
      expect(actual).to.be.true
      done()
    })

    it('should return false if the stack is not empty', done => {
      const stack = new Stack([1,2])
      const actual = stack.isEmpty()
      expect(actual).to.be.false
      done()
    })
  })

  describe('Empty', () => {
    it('should empty the stack', done => {
      const stack = new Stack([1,2])
      const actual = stack.empty()
      expect(actual).to.be.deep.eq([])
      done()
    })
  })
  
  describe('sortBy', () => {
    it('should sort the elemets on the stack by attribute given', done => {
      const stack = new Stack([{ id: 1 }, { id: 4 }, { id: 2 }, { id: 3 }])
      const expected = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }]
      const actual = stack.sortBy('id')
      expect(actual).to.be.deep.eq(expected)
      const element = stack.getAll()
      expect(element).to.be.deep.eq(expected)
      done()
    })
    
    it('shoud throw error if property does not exist', done => {
      const stack = new Stack([1])
      expect(() => stack.sortBy('banana')).to.throw('attribute banana does not exist')
      done()
    })
  })
})